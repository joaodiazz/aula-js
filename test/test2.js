let prompt = require("prompt-sync")();

let num1 = parseFloat(prompt("Informe o primeiro número: "));
let num2 = parseFloat(prompt("Informe o segundo número: "));

let soma = num1 + num2;

console.log(`A soma de ${num1} e ${num2} é: ${soma}`);
