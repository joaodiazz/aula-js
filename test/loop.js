for (let i = 0; i < 10; i++) {
  console.log(i + 1);
}

console.log("\n");

let i = 0;
while (i < 10) {
  console.log(`i agora é: ${i}`);
  i++;
}

console.log("\n");

let j = 0;
do {
  j++;
  console.log(j);
} while (j < 7);
