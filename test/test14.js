let prompt = require("prompt-sync")();

let arr = [];

do {
  let res = parseInt(prompt("Digite um número: "));
  arr.push(res);
} while (arr.length < 10);

console.log("\n");

const final = (nums) => {
  nums.map((num) => {
    if (num % 2 == 0) {
      console.log(`${num} é um número par.`);
    } else {
      console.log(`${num} é um número impar.`);
    }
  });
};

final(arr);

/////////////////////////////////////////////////////////////////

// ! o problema: Ler 10 números e imprimir quantos são pares e quantos são ímpares.

// para resolver esse problema eu tive que pensar um pouco, primeiro
// fiz um código muito grande e poluido porém funcional que não gostei muito,
// então testei com o "do while" com um "array" e funcionou perfeitamente.

// dividi o problema em dois, queria pedir 10 números ao user sem ter muito
// código e por fim fazer a verificação se era um número par ou impar.

// usei o "do while" para pedir um número ao user até o "array" ter tamanho 10,
// toda vez que o user digitar um número o valor será atribuido ao "array" com
// o metodo "push". quando o tamanho do "array" for 10 o código entra em uma função que criei.

// a função recebe um "array" e na função o "array" recebe o metodo "map" que faz
// algo para cada valor do "array", nesse caso vai verificar se é impar ou par usando
// a mesma logica do test12.js
