for (i = 1; i <= 100; i++) {
  if (i % 2 == 0) {
    console.log(i);
  }
}

/////////////////////////////////////////////////////////////////

// ! o problema: encontrar todos os números pares entre 1 e 100.

// para resolver o problema usei um padrão que já tinha usado a
// algum tempo atrás, fiz um "loop for" para contar de 1 a 100 com
// uma condição: se o índice dividido por 2 restar 0 imprima o
// índice. todo valor imprimido sera um número par.
