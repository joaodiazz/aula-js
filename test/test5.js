let prompt = require("prompt-sync")();

console.log("Coversor de metros para centímetros\n");

let metros = prompt("Digite o valor em metros: ");

let cm = metros * 100;

console.log(`${cm}cm`);

/////////////////////////////////////////////////////////////////

// ! o problema: converter metros para centímetros.

// pesquisei um pouco e a formula para fazer isso é pegar o valor em metro e multiplicar por 100. (sim tinha esquecido como fazer isso)

// com isso em mente, dividi o problema pedindo para o user
// digitar o valor que ele queira converter, armazenei o valor
// em uma variavel e fiz a formula e mostrei o resultado.
