let prompt = require("prompt-sync")();

console.log("Tabuada de Multiplicação.");
let resInt = parseInt(prompt("Digite um número de 1 a 10: "));

const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
if (resInt <= 10 && resInt > 0) {
  return numeros.map((numero) => {
    console.log(resInt * numero);
  });
} else {
  console.log("\nVALOR INVALIDO!!!");
  console.log("Digite um valor entre 1 e 10.");
}
